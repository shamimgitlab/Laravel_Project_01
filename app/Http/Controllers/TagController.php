<?php

namespace App\Http\Controllers;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;
use Illuminate\Database\QueryException;
class TagController extends Controller
{

    public function home()
    {
        return view('backend.home');
    }

    public function index()
    {
        $tags = Tag::latest()->get();
        return view('backend.tags.index', [
            'tags' => $tags
        ]);
    }

    public function create()
    {
        return view('backend.tags.create');
    }

    public function store(TagRequest $request)
    {

        try {
            
            Tag::create([
                'title' => $request->title,
            
            ]);

            return redirect()->route('tags.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Tag $tag)
    {
        return view('backend.tags.show', [
            'tag' => $tag
        ]);
    }

    public function edit(Tag $tag)
    {
        return view('backend.tags.edit', [
            'tag' => $tag
        ]);
    }

    public function update(TagRequest $request, Tag $tag)
    {
        try {
          
            $tag->update([
                'title' => $request->title
                
            ]);
           
            return redirect()->route('tags.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
           
        }
    }

    public function destroy(Tag $tag)
    {
        try {
            $tag->delete();
            return redirect()->route('tags.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            
        }
    }


}
