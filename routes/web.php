<?php

use App\Http\Controllers\TagController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\WelcomeController;

use Illuminate\Support\Facades\Route;



Route::get('/', function(){
    return view('backend.home');
});


Route::get('/home', function(){
    return view('backend.home');
});




// Route::get('/categories', [CategoryController::class, 'index'])->name('categories.index');
// Route::get('/categories/create', [CategoryController::class, 'create'])->name('categories.create');
// Route::post('/categories', [CategoryController::class, 'store'])->name('categories.store');
// Route::get('/categories/{category}', [CategoryController::class, 'show'])->name('categories.show');
// Route::get('/categories/{category}/edit', [CategoryController::class, 'edit'])->name('categories.edit');
// Route::patch('/categories/{category}', [CategoryController::class, 'update'])->name('categories.update');
// Route::delete('/categories/{category}', [CategoryController::class, 'destroy'])->name('categories.destroy');


Route::get('/home', [TagController::class, 'home'])->name('backend.home');

Route::get('/tags', [TagController::class, 'index'])->name('tags.index');

Route::get('/tags/create', [TagController::class, 'create'])->name('tags.create');

Route::post('/tags', [TagController::class, 'store'])->name('tags.store');

Route::get('/tags/{tag}', [TagController::class, 'show'])->name('tags.show');

Route::get('/tags/{tag}/edit', [TagController::class, 'edit'])->name('tags.edit');

Route::patch('/tags/{tag}', [TagController::class, 'update'])->name('tags.update');

Route::delete('/tags/{tag}', [TagController::class, 'destroy'])->name('tags.destroy');


