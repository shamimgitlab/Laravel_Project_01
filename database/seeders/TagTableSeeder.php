<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <10 ; $i++) { 
            \App\Models\Tag::create([
                'title' =>'Learn Laravel with Pondit'
            ]);
            \App\Models\Tag::create([
                'title' =>'PHP with Laravel'
            ]);
        }

        
    }
}
